# Imágen basada  en la oficial de Node.js, la guardo como variable para ser usada en nginx
FROM node:12-buster-slim as build-stage
WORKDIR /app
# Copio package.json y package-lock.json al contenedor para instalar las dependencias en el siguiente paso
COPY package*.json /app/
RUN npm install
# Instaladas las dependencias, copio el código generado en el workdir
COPY ./ /app/
# Uso la config de Angualar CLI, en este caso producción como predeterminado.
ARG configuration=production
# Como el contenedor no tiene angular CLI de forma global, uso npm para correr el build; y le paso el parámetro
# de arriba como configuración para optimizar el build; meto lo generado en dist/out para no tener en cuenta el nombre
# del proyecto en la carpeta generada
RUN npm run build -- --output-path=./dist/out --configuration $configuration


# Paso 2, uso sólo la salida compilada con Nginx para poder ser desplegada.
FROM nginx:1.18-alpine
# Leo la variable build-stage (lo hecho en el paso anterior)
COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]