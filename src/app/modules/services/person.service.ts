import { HttpClient } from '@angular/common/http';
import { Person } from '../models/person.model';
import { Injectable } from '@angular/core';

@Injectable()

export class PersonService {
  url = 'api/person/GetPersonsByRole/';

  public constructor(private http: HttpClient) {
  }

  public getPersonByRole(idRole: number) {
    return this.http.get<Person[]>(this.url +  + idRole);
  }
}
