export class Person {
  id: number;
  firstname: string;
  lastname: string;
  role: number;

  constructor(
    id?: number,
    firstName?: string,
    lastName?: string,
    role?: number
  ) {
    this.id = id;
    this.firstname = firstName;
    this.lastname = lastName;
    this.role = role;
  }
}
