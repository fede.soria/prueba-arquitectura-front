import { Component, OnInit } from '@angular/core';
import { Person } from './modules/models/person.model';
import { PersonService } from './modules/services/person.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  lsPerson: Person[] = [];

  constructor(
    private personService: PersonService
  ) {
  }

  ngOnInit() {
  }

  public getPeople(idRole: number): void {
    this.personService.getPersonByRole(idRole).subscribe((persons) =>
      this.lsPerson = persons
    );
  }
}
